FROM homebrew/brew:latest
ENV DEBIAN_FRONTEND=noninteractive
USER root 

RUN apt update -y --allow-insecure-repositories && \
    apt install -y python3 cmake && \
    apt install -y git python-is-python3 jq kinit xrootd-client && \
    apt install -y python3-pip shellcheck curl tcl tcllib yamllint wget krb5-user sssd-krb5 libtinfo6 build-essential procps file zip

    
RUN pip3 install --upgrade pip && \
    pip3 install setuptools && \
    pip3 install requests envparse && \
    pip3 install anybadge lxml jinja2 pyyaml 

RUN wget https://sourceforge.net/projects/nagelfar/files/Rel_133/nagelfar133.tar.gz
RUN tar -xvf nagelfar133.tar.gz
RUN mkdir /scripts
RUN ls -lrt
RUN mv nagelfar133 /scripts/nagelfar133

RUN wget https://sourceforge.net/projects/doxygen/files/rel-1.8.17/doxygen-1.8.17.linux.bin.tar.gz
RUN tar -xvzf doxygen-1.8.17.linux.bin.tar.gz
RUN mv doxygen-1.8.17 /scripts/doxygen
# RUN NONINTERACTIVE=1 /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
# RUN (echo; echo 'eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"') >> /root/.bashrc
# RUN eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"

USER linuxbrew
RUN brew install glab gh 
USER root

COPY hog-yaml.yaml /scripts/

# RUN useradd -c 'clang-format-bot' -m -d /home/clang -s /bin/bash clang
# USER clang
# ENV HOME /home/clang

CMD bash
